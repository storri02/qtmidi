#ifndef QMIDISYSTEMPLUGIN_H
#define QMIDISYSTEMPLUGIN_H

#include <QtCore/qstring.h>
#include <QtCore/qplugin.h>

#include <QtMidi/qtmididefs.h>
#include <QtMidi/qmidi.h>
#include <QtMidi/qmidisystem.h>

QT_BEGIN_NAMESPACE

struct Q_MIDI_EXPORT QMidiSystemFactoryInterface
{
    virtual QList<QByteArray> availableDevices(QMidi::Mode) const = 0;
    virtual QAbstractMidiInput* createInput(const QMidiDeviceInfo& device) = 0;
    virtual QAbstractMidiOutput* createOutput(const QMidiDeviceInfo& device) = 0;
    virtual QString deviceName(const QByteArray& device, QMidi::Mode mode) = 0;
    virtual ~QMidiSystemFactoryInterface();
};

#define QMidiSystemFactoryInterface_iid \
    "org.qt-project.qt.midisystemfactory/5.0"
Q_DECLARE_INTERFACE(QMidiSystemFactoryInterface, QMidiSystemFactoryInterface_iid)

class Q_MIDI_EXPORT QMidiSystemPlugin : public QObject, public QMidiSystemFactoryInterface
{
    Q_OBJECT
    Q_INTERFACES(QMidiSystemFactoryInterface)

public:
    explicit QMidiSystemPlugin(QObject* parent = Q_NULLPTR);
    ~QMidiSystemPlugin();

    virtual QList<QByteArray> availableDevices(QMidi::Mode) const = 0;
    virtual QAbstractMidiInput* createInput(const QMidiDeviceInfo& device) = 0;
    virtual QAbstractMidiOutput* createOutput(const QMidiDeviceInfo& device) = 0;
    virtual QString deviceName(const QByteArray& device, QMidi::Mode mode) = 0;

    QMidiDeviceInfo deviceInfo(const QString &realm, const QByteArray &handle, QMidi::Mode mode);

    void notifyInputDeviceAttached(const QMidiDeviceInfo& deviceInfo);
    void notifyOutputDeviceAttached(const QMidiDeviceInfo& deviceInfo);
    void notifyInputDeviceDetached(const QMidiDeviceInfo& deviceInfo);
    void notifyOutputDeviceDetached(const QMidiDeviceInfo& deviceInfo);

    void notifyDeviceAttached(const QMidiDeviceInfo& deviceInfo, QMidi::Mode mode);
    void notifyDeviceDetached(const QMidiDeviceInfo& deviceInfo, QMidi::Mode mode);
};

QT_END_NAMESPACE

#endif // QMIDISYSTEMPLUGIN_H
